import React, { useState } from 'react'
import { Button } from '@mui/material'

const HeaderButton = ({ handleChange, name, id, size }) => {
    const [hover, setHover] = useState()
    return (
        <Button onMouseEnter={() => setHover(true)} onMouseLeave={() => setHover(false)} sx={{ width: `${size}px` }}
            onClick={() => { handleChange( id ) }}
            variant={hover ? "contained" : "outlined"}
            color={hover ? "secondary" : "primary"}>{name}</Button>
    )
}

export default HeaderButton