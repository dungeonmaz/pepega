import { Box, Typography, Toolbar, AppBar, Button, Popover } from '@mui/material'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import HeaderButton from './HeaderButton'

const Header = ({ matches }) => {
    const navigate = useNavigate()
    const [anchorEl, setAnchorEl] = useState(null);

    const handleChange = (path) => {
        switch (path) {
            case "/":
                navigate("/")
                break
            case "/projects":
                navigate("/projects")
                break
            case "/houses":
                navigate("/houses")
                break
            case "/arbors":
                navigate("/arbors")
                break
            case "/saunas":
                navigate("/saunas")
                break
            default:
                break
        }
    }

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar sx={{ display: 'flex', flexDirection: matches ? 'row' : 'column', alignItems: 'center', background: '#444444' }}>
                <Toolbar>
                    <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                        <Typography variant='h5' mr={4} color="primary">Трострой</Typography>
                        {matches ? <Typography variant='body2' color="secondary">Строительство домов и бань из бруса</Typography> : null}
                    </Box>
                    {matches ? 
                    <Box sx={{ display: 'flex', flexDirection: 'row', gap: '8px' }}>
                        <HeaderButton size={120} name={"Главная"} id={"/"} handleChange={handleChange} />
                        <HeaderButton size={120} name={"Проекты"} id={"/projects"} handleChange={handleChange} />
                        <HeaderButton size={120} name={"Дома"} id={"/houses"} handleChange={handleChange} />
                        <HeaderButton size={120} name={"Беседки"} id={"/arbors"} handleChange={handleChange} />
                        <HeaderButton size={120} name={"Бани"} id={"/saunas"} handleChange={handleChange} />
                    </Box> :
                        <div>
                            <Button aria-describedby={id} variant="contained" color='secondary' onClick={handleClick}>
                                Навигация
                            </Button>
                            <Popover
                                id={id}
                                open={open}
                                anchorEl={anchorEl}
                                onClose={handleClose}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                }}
                            >
                                <Box sx={{ display: 'flex', flexDirection: 'column', gap: '8px', background: '#444444', padding: '8px' }}>
                                    <HeaderButton size={200} name={"Главная"} id={"/"} handleChange={handleChange} />
                                    <HeaderButton size={200} name={"Проекты"} id={"/projects"} handleChange={handleChange} />
                                    <HeaderButton size={200} name={"Дома"} id={"/houses"} handleChange={handleChange} />
                                    <HeaderButton size={200} name={"Беседки"} id={"/arbors"} handleChange={handleChange} />
                                    <HeaderButton size={200} name={"Бани"} id={"/saunas"} handleChange={handleChange} />
                                </Box>
                            </Popover>
                        </div>}
                </Toolbar>
            </AppBar>
            <Toolbar />
        </Box>
    )
}

export default Header