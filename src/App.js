import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Footer from './components/footer/Footer'
import Header from './components/header/Header'
import Main from './components/main/Main'
import { useMediaQuery } from '@mui/material';

const App = () => {
  const matches = useMediaQuery('(min-width:600px)')
  return (
    <div>
      <BrowserRouter>
        <Header matches={matches}/>
        <Routes>
          <Route path='/' element={<Main/>}/>
          <Route path='/projects' element={<Main/>}/>
          <Route path='/houses' element={<Main/>}/>
          <Route path='/arbors' element={<Main/>}/>
          <Route path='/saunas' element={<Main/>}/>
        </Routes>
        <Footer/>
      </BrowserRouter>
    </div>
  )
}

export default App